/*
 * Copyright (C) 2023 Cobblemon Contributors
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.cobblemon.mod.common.cobblemonstructures;

public class CobblemonStructureIDs {
    //Location of the nbt
    //Not sure if this is a good way to store them but doing this meant I wouldnt have to keep typing this out during development
    public static final String PLAINS_POKECENTER = "cobblemon:village_plains/village_plains_pokecenter";
    public static final String DESERT_POKECENTER = "cobblemon:village_desert/village_desert_pokecenter";
    public static final String SAVANNA_POKECENTER = "cobblemon:village_savanna/village_savanna_pokecenter";
    public static final String SNOWY_POKECENTER = "cobblemon:village_snowy/village_snowy_pokecenter";
    public static final String TAIGA_POKECENTER = "cobblemon:village_taiga/village_taiga_pokecenter";

    public static final String PLAINS_LONG_PATH = "cobblemon:village_plains/village_plains_long_path";
    public static final String DESERT_LONG_PATH = "cobblemon:village_desert/village_desert_long_path";
    public static final String SAVANNA_LONG_PATH = "cobblemon:village_savanna/village_savanna_long_path";
    public static final String SNOWY_LONG_PATH = "cobblemon:village_snowy/village_snowy_long_path";
    public static final String TAIGA_LONG_PATH = "cobblemon:village_taiga/village_taiga_long_path";
}
